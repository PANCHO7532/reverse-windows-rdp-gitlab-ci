# Reverse Windows RDP for GitLab CI by PANCHO7532
# Based on the work of @nelsonjchen
# This script is executed when GitLab CI/CD is initialized and we have a runner available.
Write-Output "[INFO] Script started!"

# First we download ngrok
#Invoke-WebRequest -Uri https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-windows-amd64.zip -OutFile ngrok.zip
Invoke-WebRequest -Uri https://bin.equinox.io/c/bNyj1mQVY4c/ngrok-v3-stable-windows-amd64.zip -OutFile ngrok.zip

# Then we unzip it
Expand-Archive -LiteralPath '.\ngrok.zip'

# Set-up and save ngrok authtoken
./ngrok/ngrok.exe authtoken $env:NGROK_AUTH_TOKEN

# Enabling RDP Access
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server'-name "fDenyTSConnections" -Value 0

# Authorize RDP Service from firewall so we don't get a weird state
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"

# Change password to the one we set-up as RDP_PASSWORD on our repo settings.
Set-LocalUser -Name "Administrator" -Password (ConvertTo-SecureString -AsPlainText "$env:RDP_PASSWORD" -Force)

# Enable Account
Enable-LocalUser -Name "Administrator"

# Getting Web Browser (Chromium)
# Actually i don't know for how much time this link will last, i'll think of a solution later.
# Chromium Path %localappdata%\Chromium\Application\chrome.exe
Invoke-WebRequest -Uri https://storage.googleapis.com/chromium-browser-snapshots/Win_x64/908657/mini_installer.exe -OutFile 'C:\GitLab-Runner\installchromium.exe'
node ./scripts/chromium-launcher.js
Exit