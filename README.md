# Reverse Windows RDP for GitLab CI
Enable RDP access on GitLab CI/CD VMs for general use.

![xd](screenshot.png)

## Warning
You won't get an full graphical environment as you might appreciate in the screenshot, as the Windows Server instance used in the runners is on Core level. Check "Tricks and issues" section of this README.md for a few tips

(also it takes a rough 5-10 minutes to boot up, so grab a coffee and have some patience.)

## Usage
1) Create an account on [ngrok](https://dashboard.ngrok.com/signup), and copy your auth token displayed [here](https://dashboard.ngrok.com/auth)
2) Fork this repository
3) Go to Settings > CI/CD, expand "Runners" and be sure that the option "Enable shared runners for this project" is checked
4) On the same page, expand "Artifacts", uncheck "Keep artifacts from most recent successful jobs"
5) On the same page, expand "Variables", and click on "Add variable", create the following variables:
```
Key: NGROK_AUTH_TOKEN
Value: (obviously you insert the ngrok auth token of your account that you obtained in step 1)
Flags: Protect, Mask

Key: RDP_PASSWORD
Value: (The RDP password that you will be using, it must contain uppercase and lowercase characters, numbers and symbols, example: Password!#!)
Flags: Protect, Mask
```
All of those variables are required for the script to work, if you forget to add one, then it will throw an error

6) Trigger an build, by editing this README or uploading anything to your repository, don't modify the contents of the resources or scripts folders
7) Go to CI/CD > Jobs, open the running job (might be labeled in blue), and wait until the last step where it will hang forever
8) Visit ngrok's tunnel list [dashboard](https://dashboard.ngrok.com/status/tunnels)
9) Take note of the active tunnel host and port
10) Connect to the host and port combination with an RDP client of your preference
11) As username, write "Administrator" and as password, the password that you wrote in the RDP_PASSWORD secret in step 5
12) Once connected, you would have an fully working command prompt!

## Duration of the runners
You can see how long the runner will last on CI/CD > Jobs, click on your active job and near the log you will see a column with the details of the job, the value under Timeout defines how long it will last (generally for one hour).

# Tricks and issues
### Please read it carefully

- There are not web browsers by default but i managed to include an installer for chromium, type on the command line `installchromium` for install Chromium, if you accidentaly closed it, type `chromium` for open it again.
- Most apps (mainly graphical apps) can't be installed due to lack of many many packages as it's the Core version of Windows Server, if you step on an error while installing/running something, don't be surprised.
- If you close the command line accidentaly, use the combination CTRL + SHIFT + ESC for open the Task Manager and open it again. (you can open other apps from there too)
- There are not file managers as explorer.exe isn't present on the Core version, luckily you can use 7-Zip (pre-installed by GitLab) for browse through the file-system, launch it from the Task Manager by typing `7zFM.exe` on the Run section.
- As explorer.exe isn't available, you can't browse for files, and as you might guess, you can't upload and select files to an website using chromium. Drag and Drop features also don't work for the same reason.
- Graphical apps (if you managed to install one correctly) might be heavily glitched, you should've already noticed this while using chromium.